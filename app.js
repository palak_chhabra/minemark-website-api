
var createError = require('http-errors');
var express = require('express');

var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var bodyParser=require("body-parser");
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth');
var aboutRouter = require('./routes/about');
var blogRouter = require('./routes/blog');
var careerRouter = require('./routes/career');
var eventRouter = require('./routes/event');
var featureRouter = require('./routes/feature');
var postRouter = require('./routes/post');
var productRouter = require('./routes/product');
var sliderRouter = require('./routes/slider');
var userRouter = require('./routes/user');
var videoRouter = require('./routes/video');
var downloadRouter = require('./routes/download');
var servicesRouter = require('./routes/services');
var passwordRouter = require('./routes/password');
var pricingRouter = require('./routes/pricing');

var app = express();

app.use((req, res, next) => {
 
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );

  next();
});


// SET STORAGE


// view engine setup
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true,limit:'50mb' }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/about', aboutRouter);
app.use('/blog', blogRouter);
app.use('/career', careerRouter);
app.use('/event', eventRouter );
app.use('/feature', featureRouter);
app.use('/post', postRouter);
app.use('/product', productRouter);
app.use('/slider', sliderRouter);
app.use('/user', userRouter );
app.use('/video', videoRouter);
app.use('/download', downloadRouter);
app.use('/services', servicesRouter);
app.use('/password', passwordRouter);
app.use('/pricing', pricingRouter);


//catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});





module.exports = app;
