var express = require('express');
var router = express.Router();
var connection = require('../conn');
var crypto = require('crypto');

var mime = require('mime');
var multer = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      console.log(file)
      cb(null, 'public/images/about')
    },
    filename: function (req, file, cb) {
      crypto.pseudoRandomBytes(16, function (err, raw) {
        cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
      });
    }
  });
var upload = multer({ storage: storage });
//							
router.post('/Upload_About',upload.single('image'), (req, res, next) => {
  let imagePath = req.file;
  let imageaPathdb = imagePath.path;
  let imageaPathdbnew = imageaPathdb.replace("public/", "");
  console.log(req.body);
    var sql="SELECT * FROM tbl_auth WHERE id = '"+req.body.id+"'";
    connection.query(sql,function(err,user){
      if(req.body.token == user[0].token){
        console.log("valid")
              connection.connect(function(err) {
                
                var sql = "INSERT INTO `tbl_about`(`abt_name`, `abt_image`,`abt_content`,`abt_status`) VALUES ('"+req.body.abt_name+"', '"+imageaPathdbnew+"','"+req.body.abt_content+"', '"+req.body.abt_status+"')";
                connection.query(sql, function (err, result) {
                  if(err){
                    res.json({error:"someting went to wrong"})
                  }else{
                  if(result){
                    res.json({
                      msg:"Success",
                      success:"Data upload succesfully...!"
                    });
                  console.log("About us  record inserted");
                  }
                }
                });
              });	
      }else{
        res.send({error:"token invalid"});
      }
    })
});



router.post('/update_About', upload.single('image'), (req, res, next) => {
  console.log(req.body);
  var sql="SELECT * FROM tbl_auth WHERE id = '"+req.body.id+"'";
  connection.query(sql,function(err,user){
    if(req.body.token == user[0].token){
      console.log("valid")
      connection.connect(function(err) {
        if(req.file){
          let imagePath = req.file;
          let imageaPathdb = imagePath.path;
          let imageaPathdbnew = imageaPathdb.replace("public/", "");
          var sql = "UPDATE `tbl_about` SET abt_name = '"+req.body.abt_name+"', abt_image = '"+imageaPathdbnew+"',abt_content='"+req.body.abt_content+"', abt_status = '"+req.body.abt_status+"' WHERE abt_id = '"+req.body.abt_id+"'";
          connection.query(sql, function (err, result) {
            if(err){
              res.json({error:"someting went to wrong"})
            }else{
            if(result){
              res.json({
                msg:"Success",
                success:"Data upload succesfully...!"
              });
            console.log("About us record updated");
            }
          }
          });
        }else{
          var sql = "UPDATE `tbl_about` SET abt_name = '"+req.body.abt_name+"',abt_content='"+req.body.abt_content+"', abt_status = '"+req.body.abt_status+"' WHERE abt_id = '"+req.body.abt_id+"'";
          connection.query(sql, function (err, result) {
            if(err){
              res.json({error:"someting went to wrong"})
            }else{
            if(result){
              res.json({
                msg:"Success",
                success:"Data upload succesfully...!"
              });
            console.log("About us record updated");
            }
          }
          });
        }
    
      });	
           
    }else{
      res.send({error:"token invalid"});
    }
  });
});



router.post('/delete_About',  (req, res, next) => {
console.log("hiii")
  console.log(req.body.abt_id);
   var sql="SELECT * FROM tbl_auth WHERE id = '"+req.body.id+"'";
  connection.query(sql,function(err,user){
    if(req.body.token == user[0].token){
      console.log("valid");
      connection.connect(function(err) {
    
        var sql = "DELETE FROM tbl_about WHERE abt_id = '"+req.body.abt_id+"'";
        connection.query(sql, function (err, result) {
          if(err) throw err;
        console.log(err);
          console.log('Data received from Db:\n');
          console.log(result);
          res.send({success:"success",
        msg:"record deleted successfully"});
        });
      });
    }else{
      res.send({error:"token invalid"});
    }

  });	
});




  
  router.post('/single_About',  (req, res, next) => {
    console.log('hello')
    console.log(req.body.abt_id);
    var sql="SELECT * FROM tbl_auth WHERE id = '"+req.body.id+"'";
    connection.query(sql,function(err,user){
      if(req.body.token == user[0].token){
        console.log("valid");
        connection.connect(function(err) {
       
          var sql ="SELECT * FROM tbl_about WHERE abt_id = '"+req.body.abt_id+"'";
          connection.query(sql, (err,rows) => {
            if(err) throw err;
          
            console.log('Data received from Db:\n');
            console.log(rows);
            res.send(rows);
          });
        
        });	
      }else{
        res.send({error:"token invalid"});
      }
    }); 
   });


router.post('/show_About',  (req, res, next) => {
 console.log('hello')
 var sql="SELECT * FROM tbl_auth WHERE id = '"+req.body.id+"'";
 connection.query(sql,function(err,user){
   if(req.body.token == user[0].token){
     console.log("valid");
     connection.connect(function(err) {
      var sql = "SELECT * FROM tbl_about";
      connection.query(sql, (err,rows) => {
        if(err) throw err; 
        console.log('Data received from Db:\n');
        console.log(rows);
        res.send(rows);
      });
    });	
   }else{
    res.send({error:"token invalid"});
   }
  });	
});


router.get('/About_us',  (req, res, next) => {
  console.log('hello')

   connection.connect(function(err) {

   var sql = "SELECT * FROM tbl_about WHERE abt_status = '1'";
   connection.query(sql, (err,rows) => {
     if(err) throw err;
     console.log('Data received from Db:\n');
     console.log(rows);
     res.send(rows);
   });
 });	
 });

module.exports = router;